import React from 'react'
import Markdown from '../../common/components/Markdown'
import {withRouter} from 'react-router-dom'
import Breadcrumb from '../../common/components/Breadcrumb'
import styled from 'styled-components'
const Wrapper = styled.div`
  .page-content{
    background:white;
    padding:16px 21px 32px;
      h2{
        font-size:20px;
      }
  }
`
export default withRouter(({location})=>{
  const platformTerms = 'Terms & Conditions not included with this build.'
  return (<Wrapper className="screen-content">
    <Breadcrumb links={[{label:'Terms & conditions', to:location.pathname}]}/>
    <div className="page-content">
      <Markdown source={platformTerms}/>
    </div>
  </Wrapper>)
})
