import React from 'react';
import classnames from 'classnames'
import Control from './Control'
export default ({name, label, placeholder, className, required, form, maxLength, minLength, pattern, patternLabel, tooltip, disabled}) => {
  const {errors} = form
  let error = errors[name]
  const inputClassName = classnames('form-control', {error})
  patternLabel = patternLabel || `${label} is not valid`
  className = classnames(className, {error})
  return (<Control className={className} label={label} name={name} patternLabel={patternLabel} error={error} tooltip={tooltip}>
    <input
      id={name}
      placeholder={placeholder}
      className={inputClassName}
      name={name}
      ref={form.register({required, maxLength, minLength, pattern})}
      maxLength={maxLength}
      disabled={disabled}
    />
  </Control>)
}
