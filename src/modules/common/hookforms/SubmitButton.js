import React from 'react'
import Spinner from '../components/Spinner'
import {Button} from 'react-bootstrap'

const SubmitButton = ({model, label, pending, primary, onClick}) => {
  if(!label){
    label = 'Save'
  }
  const handleClick = () => {
    if (onClick) onClick()
  }
  let disabled = pending // NOTE: if pending then always disabled, otherwise based on form validation
  let bsStyle = primary?'primary':'default'
  return(<Button type="submit" variant="primary" bsStyle={bsStyle} disabled={disabled} onClick={handleClick}>
    <span>{label}</span>
    {pending && <Spinner className="spinner" dark={true} size={20}/>}
  </Button>)
}
export default SubmitButton
