import React, {useState, useEffect} from 'react';
import Control from './Control'
import {getValue} from './utils'
import WysiwygControl from '../components/Wysiwyg'
export default ({name, label, placeholder, className, required, form, setValue, tooltip}) => {
  const defaultValue = getValue(form, name)
  const [text, setText] = useState(defaultValue)
  const {errors} = form
  let error = errors[name]
  const onChange = (t)=>{
    form.setValue(name, t)
    setText(t)
  }
  useEffect(() => {
    form.register({ name }, {required})
  }, [name]);
  return (<Control label={label} name={name} error={error} tooltip={tooltip}>
    <WysiwygControl value={text} onChange={onChange}/>
  </Control>)
}
