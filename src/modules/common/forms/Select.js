import React from 'react';
import { Control } from 'react-redux-form';
import _ from 'lodash'
const Input = ({model, label, placeholder, className, required, options}) => {
  if(!className){
    className = ''
  }
  if(!label){
    label = 'Save'
  }
  const displayOptions = _.map(options, o =>{
    const value = o.value? o.value: o
    const label = o.label? o.label: o.toString()
    return <option value={value}>{label}</option>
  })
  return (<div className="form-group row">
        <label className="col-sm-2 col-form-label">{label}</label>
        <div className="col-sm-10">
          <Control.select model={model} className="form-control" placeholder={placeholder} required={required} children={displayOptions}/>
        </div>
      </div>)
}
export default Input
