import React from 'react'
import styled from 'styled-components'
import {Link, withRouter} from 'react-router-dom'
import { version } from '../../../package.json'
const View = styled.footer `
  width: 100%;
  background: ${props => props.theme.colors.white};
  flex-shrink: 0;
  & .footer-bottom{
    background: ${props => props.theme.colors.white};
    color: ${props => props.theme.colors.black}
    font-size:10px;
    padding: 10px 30px;
    text-align:center;
    & a{
      color: ${props => props.theme.colors.black}
      & :hover{
        color: ${props => props.theme.colors.white}
      }
      & :active{
        color: ${props => props.theme.colors.white}
      }
    }
    .powered{
      a{
        display:inline-block;
        background-image:url('/images/powered_by_cogniss.png');
        background-size:150px 41px;
        width:150px;
        height:41px;
        text-indent:-100em;
        overflow:hidden;
      }
    }
    .version{
      margin-bottom:20px;
      color: ${props => props.theme.colors.offWhite}
    }
    @media (min-width: 768px) {
      display:flex;
      p{
        display:inline-block;
        margin-right:10px;
        flex: 0 0 auto;
        &.version{
          flex: 1 0 auto;
          text-align:center;
          margin-bottom:10px;
        }
      }
    }
  }
`

const Footer = ({location, match})=>{
  let privacyLink = "/privacy"
  let termsLink = "/terms"
  return (<View>
    <div className="footer-bottom">
        <p>
          <Link target='_blank' to={privacyLink}>
            Privacy policy
          </Link>&nbsp;
        </p>
        <p>
          <Link target='_blank' to={termsLink}>
            Terms and conditions
          </Link>
        </p>
        <p className="version">
          platform version {version}
        </p>
        <p className="powered">
          <a target='_blank' rel="noopener noreferrer" href="https://cogniss.com">Powered by Cogniss</a>
        </p>
      </div>
  </View>)
}

export default withRouter(Footer)
